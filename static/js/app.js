var mxBasePath = 'static/js/mxgraph/src';
window.onload = function () {
    loadMxGraph();
};
function log(content) {
    var logger = document.getElementById('logContainer'),
    para = document.createElement('p'),
    contentNode = document.createTextNode(content);
    para.innerHTML = content;
    logger.appendChild(para);
    logger.scroll(0, logger.scrollHeight);
}