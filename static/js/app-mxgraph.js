function loadMxGraph() {
    var container = document.getElementById('graphContainer'),
        model = new mxGraphModel(),
        graph = new mxGraph(container, model),
        parent = graph.getDefaultParent(),// Gets the default parent for inserting new cells. This is normally the first child of the root (ie. layer 0).
        widthFactor = window.innerWidth/1200,
        vInternet,
        vFacebook,
        vLetter,
        vSms,
        userObject = {
            href: 'http://www.google.com',
            path: 'http://www.google.com',
            nextAction: 'http://www.google.com',
            label: 'label set in JSON data!',
            toString: function () {
                return this.label;
            }
        },
        vSmartPhone,
        e1,
        e2,
        e3;

    // Disables the built-in context menu
    mxEvent.disableContextMenu(container);

    // Enables rubberband selection
    new mxRubberband(graph);
    configureStylesheet(graph);


    graph.addListener(mxEvent.CLICK, function(sender, evt)
    {
        var e = evt.getProperty('event'); // mouse event
        var cell = evt.getProperty('cell'); // cell may be null
        var logMessage = "<div class='logRow'>[Clicked] ";
        console.log(['click', cell]);
        window.appMxCell = cell;
        if (cell != null)
        {
            logMessage += getCellDetailsAsString(cell);
            // Do something useful with cell and consume the event
            evt.consume();
        }

        logMessage += '</div>';
        log(logMessage);
    });
    graph.addListener(mxEvent.DOUBLE_CLICK, function(sender, evt)
    {
        var e = evt.getProperty('event'); // mouse event
        var cell = evt.getProperty('cell'); // cell may be null
        console.log(['dbl-click', cell]);
        var logMessage = "<div class='logRow'>[Double-Clicked] ";
        window.appMxCell = cell;
        if (cell != null)
        {
            logMessage += getCellDetailsAsString(cell);
            // Do something useful with cell and consume the event
            evt.consume();
        }
        logMessage += '</div>';
        log(logMessage);
    });

    // Adds cells to the model in a single step
    graph.getModel().beginUpdate();
    try
    {
        vInternet = graph.insertVertex(parent, null, '', 300 * widthFactor, 10, 100 * widthFactor, 100 * widthFactor, 'internet');
        vFacebook = graph.insertVertex(parent, null, '', 100 * widthFactor, 200, 80 * widthFactor, 100 * widthFactor, 'facebook');
        vLetter = graph.insertVertex(parent, null, '', 500 * widthFactor, 50, 80 * widthFactor, 100, 'letter');
        vSms = graph.insertVertex(parent, null, 'First Line\nSecond Line', 20, 330, 140, 60, 'smartphone');
        vSmartPhone = graph.insertVertex(parent, null, userObject, 280 * widthFactor, 230 * widthFactor, 180, 60, 'sms');

        e1 = graph.insertEdge(parent, null, '', vInternet, vLetter);
        e2 = graph.insertEdge(parent, null, '', vInternet, vFacebook);
        e3 = graph.insertEdge(parent, null, '', vFacebook, vSmartPhone);

    }
    finally
    {
        // Updates the display
        graph.getModel().endUpdate();
    }
}

function getCellDetailsAsString(cell) {

    var html = '',
        edges = cell.edges || [],
        cellId = cell.id,
        i,
        connector,
        cFrom,
        cTo;

    html += cell.isEdge() ? ' a <strong>Connector</strong>' : ' a <strong>Node</strong>';
    html += '. Details: ' +
        '<strong>ID</strong>: ' + cellId +
        ', <strong>value</strong>: ' + (cell.value || '&lt;blank&gt;') +
        ', <strong>style</strong>: ' + (cell.style || '&lt;blank&gt;');

    if (typeof cell.getValue() == 'object') {
        html += ' <p>Custom JSON data (hacked!): ' + JSON.stringify(cell.getValue()) + "</p>";
    }

    if (edges.length) {
        html += "<p> HAS CONNECTOR:</p>";
        html += '<ol>';
        for(i = 0; i < edges.length; i++) {
            connector = edges[i];
            cFrom = connector.source;
            cTo = connector.target;
            if (cFrom.id == cellId) {
                html += "<li>Connector flows to Node with ID: "+ cTo.id + ' - ' + (cTo.value || cTo.style) + " </li>";
            }
            if (cTo.id == cellId) {
                html += "<li>Connector flows from Node with ID: "+ cFrom.id + ' - ' + (cFrom.value || cFrom.style) + " </li>";
            }
        }
        html += '</ol>';
    }


    return html;
}

function configureStylesheet(graph)
{
    var style = {};
    style[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    style[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
    style[mxConstants.STYLE_IMAGE] = 'static/images/marketing/internet.png';
    style[mxConstants.STYLE_FONTCOLOR] = '#FFFFFF';
    graph.getStylesheet().putCellStyle('internet', style);

    style = mxUtils.clone(style);
    style[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    style[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_CENTER;
    style[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_TOP;
    style[mxConstants.STYLE_IMAGE_ALIGN] = mxConstants.ALIGN_CENTER;
    style[mxConstants.STYLE_IMAGE_VERTICAL_ALIGN] = mxConstants.ALIGN_TOP;
    style[mxConstants.STYLE_IMAGE] = 'static/images/marketing/043-facebook-1.png';
    style[mxConstants.STYLE_IMAGE_WIDTH] = '48';
    style[mxConstants.STYLE_IMAGE_HEIGHT] = '48';
    style[mxConstants.STYLE_SPACING_TOP] = '56';
    style[mxConstants.STYLE_SPACING] = '8';
    graph.getStylesheet().putCellStyle('facebook', style);

    style = mxUtils.clone(style);
    style[mxConstants.STYLE_IMAGE_VERTICAL_ALIGN] = mxConstants.ALIGN_BOTTOM;
    style[mxConstants.STYLE_IMAGE] = 'static/images/marketing/letter.png';
    delete style[mxConstants.STYLE_SPACING_TOP];
    graph.getStylesheet().putCellStyle('letter', style);

    style = mxUtils.clone(style);
    style[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_LEFT;
    style[mxConstants.STYLE_STROKECOLOR] = '#000000';
    style[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_LABEL;
    style[mxConstants.STYLE_IMAGE_ALIGN] = mxConstants.ALIGN_LEFT;
    style[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_MIDDLE;
    style[mxConstants.STYLE_IMAGE_VERTICAL_ALIGN] = mxConstants.ALIGN_MIDDLE;
    style[mxConstants.STYLE_IMAGE] = 'static/images/marketing/smartphone.png';
    style[mxConstants.STYLE_SPACING_LEFT] = '55';
    style[mxConstants.STYLE_SPACING] = '4';
    graph.getStylesheet().putCellStyle('smartphone', style);

    style = mxUtils.clone(style);
    style[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_RIGHT;
    style[mxConstants.STYLE_IMAGE_ALIGN] = mxConstants.ALIGN_RIGHT;
    delete style[mxConstants.STYLE_SPACING_LEFT];
    style[mxConstants.STYLE_FONTCOLOR] = '#FF0000';
    style[mxConstants.STYLE_SPACING_RIGHT] = '55';
    graph.getStylesheet().putCellStyle('sms', style);
}
